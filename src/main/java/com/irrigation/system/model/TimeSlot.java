package com.irrigation.system.model;

import com.irrigation.system.enums.TimeSlotStatus;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Data
@Entity
@Table(name = "timeSlot")
public class TimeSlot {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column
    private TimeSlotStatus status;

    @Column
    private LocalDateTime time;

    @Column
    private Double timeNeeded;

    public TimeSlot() {
        super();
    }

    public TimeSlot(TimeSlotStatus status, LocalDateTime time) {
        super();
        this.status = status;
        this.time = time;
    }

}
