package com.irrigation.system.service.impl;

import com.irrigation.system.enums.PlotOfLandIrregationStatus;
import com.irrigation.system.enums.SensorStatus;
import com.irrigation.system.enums.TimeSlotStatus;
import com.irrigation.system.exception.ResourceNotFoundException;
import com.irrigation.system.model.PlotOfLand;
import com.irrigation.system.model.Sensor;
import com.irrigation.system.model.TimeSlot;
import com.irrigation.system.repository.PlotOfLandRebository;
import com.irrigation.system.service.PlotOfLandService;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class PlotOfLandServiceImpl implements PlotOfLandService {

    private final PlotOfLandRebository plotOfLandRebository;

    public PlotOfLandServiceImpl(PlotOfLandRebository plotOfLandRebository) {
        this.plotOfLandRebository = plotOfLandRebository;
    }

    @Override
    public PlotOfLand addNewPlotOfLand(PlotOfLand plotOfLand) {
        return plotOfLandRebository.save(plotOfLand);
    }

    @Override
    public PlotOfLand configurePlotOfLand(Integer id, Integer sensorId, String agriculturalCrop) {

        Optional<PlotOfLand> plotOfLand = plotOfLandRebository.findById(id);
        if (plotOfLand.get() == null) {
            throw new ResourceNotFoundException("Plot Of Land", "Id", id);
        } else {
            Sensor sensor = new Sensor();
            sensor.setId(sensorId);
            sensor.setStatus(SensorStatus.UNAVAILABLE);
            plotOfLand.get().setSensor(sensor);
            plotOfLand.get().setWaterAmount(predictAmountOfWater(agriculturalCrop, plotOfLand.get().getArea()));
            if (plotOfLand.get().getTimeslots() != null) {
                plotOfLand.get().getTimeslots().add(predictTimeSlot(plotOfLand.get().getWaterAmount()));
            } else {
                List<TimeSlot> list = new ArrayList<>();
                list.add(predictTimeSlot(plotOfLand.get().getWaterAmount()));
                plotOfLand.get().setTimeslots(list);
            }
        }

        return plotOfLandRebository.save(plotOfLand.get());
    }

    @Override
    public PlotOfLand editPlotOfLand(String status, Integer id) {
        Optional<PlotOfLand> plotOfLand = plotOfLandRebository.findById(id);
        if (plotOfLand.get() == null) {
            throw new ResourceNotFoundException("Plot Of Land", "Id", id);
        } else {
            plotOfLand.get().setStatus(PlotOfLandIrregationStatus.valueOf(status));
        }
        return plotOfLandRebository.save(plotOfLand.get());
    }

    @Override
    public List<PlotOfLand> getAllPlotOfLand() {
        return plotOfLandRebository.findAll();
    }

    @Override
    public void irrigatePlotOfLands() {
        //TODO:
    }

    private Double predictAmountOfWater(String agriculturalCrop, Double area) {
        if (agriculturalCrop.equals("tree") && area > 100)
            return 20.0;
        else if (agriculturalCrop.equals("tree") && area < 100)
            return 15.0;

        return 10.0;
    }

    private TimeSlot predictTimeSlot(double amountOfWater) {
        TimeSlot timeSlot = new TimeSlot(TimeSlotStatus.RESERVED, LocalDateTime.now());

        if (amountOfWater == 20.0)
            timeSlot.setTimeNeeded(30.0);
        else if (amountOfWater == 15.0)
            timeSlot.setTimeNeeded(25.0);
        else
            timeSlot.setTimeNeeded(15.0);
        return timeSlot;
    }
}
