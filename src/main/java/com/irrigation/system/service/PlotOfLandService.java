package com.irrigation.system.service;

import com.irrigation.system.model.PlotOfLand;

import java.util.List;

public interface PlotOfLandService {

    PlotOfLand addNewPlotOfLand(PlotOfLand plotOfLand);

    PlotOfLand configurePlotOfLand(Integer id, Integer sensorId, String agriculturalCrop);

    PlotOfLand editPlotOfLand(String status, Integer id);

    List<PlotOfLand> getAllPlotOfLand();

    void irrigatePlotOfLands();
}
