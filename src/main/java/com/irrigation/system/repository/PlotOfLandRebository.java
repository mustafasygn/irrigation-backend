package com.irrigation.system.repository;

import com.irrigation.system.model.PlotOfLand;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PlotOfLandRebository extends JpaRepository<PlotOfLand, Integer> {

}
