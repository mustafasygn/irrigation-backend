package com.irrigation.system.repository;

import com.irrigation.system.model.TimeSlot;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TimeSlotsRebository extends JpaRepository<TimeSlot, Integer> {

}
