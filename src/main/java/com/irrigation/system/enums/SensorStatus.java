package com.irrigation.system.enums;

public enum SensorStatus {
    AVAILABLE,
    UNAVAILABLE
}
