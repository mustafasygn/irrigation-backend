# Irrigation-System

![add_plot](/res/diagram.png)

### Irrigation Cron Job ###

1. Cron Job runs every 5 mins
2. Plot of Land irrigate method is called and all of the plots having time slot within next 5 mins listed
3. Loops the plots list and checks the sensor availability
4. Irrigate the land if the sensor is available otherwise try 5 more times and alert if not available at all


You can use that command for running :
  mvn spring-boot:run

if you want to test addNewPlotOfland for simplicity you can use below request 

```yaml
{
  "id" : 1,
  "status" : null,
  "area" : 4.5,
  "waterAmount" : 3.0,
  "timeslots" : null,
  "sensor" : {
    "id" : 1,
    "status" : null
  }
}
```
Add plot:
![add_plot](/res/add_plot.png)

Get All plots
![get_all_plots](/res/get_all_plots.png)
